
// This project's source code is released under the MIT License.
// - http://opensource.org/licenses/MIT

#include "ParticleBase.hpp"

ParticleBase::ParticleBase()  { }
ParticleBase::~ParticleBase() { }

void ParticleBase::ConfigParticle(float posX, float posY, float posZ, PixelBuffer *texture)
{
	x = posX;
	y = posY;
	z = posZ;
	this->texture = texture;
	life = 100;
}

void ParticleBase::Update(float deltaTime)
{
	y -= (50.0f * deltaTime);
	life--;
	Render();
}

void ParticleBase::Render()
{
	glRasterPos2f(x,y);
	glDrawPixels(texture->width, texture->height,
		((texture->bytesPerPix == 4) ? GL_BGRA_EXT : GL_BGR_EXT),
		GL_UNSIGNED_BYTE, texture->pixels);
}
