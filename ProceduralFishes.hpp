
// This project's source code is released under the MIT License.
// - http://opensource.org/licenses/MIT

#ifndef PROCEDURAL_FISHES_HPP
#define PROCEDURAL_FISHES_HPP

#include <CommLib.hpp>
#include <GfxLib.hpp>
#include <MathLib.hpp>
#include <MeshLib.hpp>

using namespace CommLib;
using namespace GfxLib;
using namespace MathLib;
using namespace MeshLib;

// The Fish
class Fish
{
	friend class FlockOfFishes;

public:

	Fish(AnimatedMesh * mesh, GLuint displayList, FlockOfFishes * flock);
	void Update(float deltaTime);
	void Render(float deltaTime);

private:

	// Deleted outside !
	AnimatedMesh * fishMesh;

	// If 'fishMesh' is null, use the display list.
	GLuint displayList;

	// Not deleted, just a reference !
	FlockOfFishes * myFlock;

	// Control variables
	Vec3f position;
	Vec3f velocity;
};

// The Flock (Group of fishes)
class FlockOfFishes
{
public:

	FlockOfFishes(const char * cfgFileName);
	~FlockOfFishes();

	void UpdateFishes(float deltaTime);
	void RenderFishes(float deltaTime);

	void GetNeighbors(Fish * fish, float radius, std::vector<Fish *> & neighbors);

private:

	static void SetNoiseTexture(AnimatedMesh * animMesh,
	                            const char * texName, float scale, int width, int seed,
	                            unsigned char r, unsigned char g, unsigned char b);

	// Array with the 'Fish' objects
	std::vector<Fish *> fishes;

	// Fish models:
	AnimatedMesh * fish1; // In folder "Fish1/"
	AnimatedMesh * fish2; // In folder "Fish2/"
	AnimatedMesh * shark; // In folder "Shark/"

	GLuint displayList1; // Display list for fish1
	GLuint displayList2; // Display list for fish2
};

#endif // PROCEDURAL_FISHES_HPP
