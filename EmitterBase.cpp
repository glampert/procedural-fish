
// This project's source code is released under the MIT License.
// - http://opensource.org/licenses/MIT

#include "EmitterBase.hpp"

EmitterBase::EmitterBase()  { }
EmitterBase::~EmitterBase() { }

EmitterBase::EmitterBase(std::list<ParticleBase*> *particles, float posX, float posY, PixelBuffer *texture)
{
	// receives poninter to particle list
	this->particles = particles;

	// init position
	x = posX;
	y = posY;

	// copy poninter
	this->texture = texture;
}

void EmitterBase::SetPos(float _x, float _y)
{
	x = _x;
	y = _y;
}

void EmitterBase::Update()
{
	int posX, posY;
	posX = (screen.width-((rand())%(screen.width*2 + 1))) + x;
	posY = (screen.width-((rand())%(screen.width*2 + 1))) + y;

	// create one particle per frame
	ParticleBase * temp = new ParticleBase();

	temp->ConfigParticle(posX, posY, 0, texture);
	particles->push_back(temp); // add on the list
}
