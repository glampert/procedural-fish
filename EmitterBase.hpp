
// This project's source code is released under the MIT License.
// - http://opensource.org/licenses/MIT

/*
	EmitterBase originally written for ParticlePlayground assignment.

	This is a simplified version for CGA class
	2011 - 2
*/

#ifndef EMITTER_BASE_CLASS
#define EMITTER_BASE_CLASS

#include <list>
#include <vector>

#include "ParticleBase.hpp"

class EmitterBase
{
public:

	 EmitterBase(std::list<ParticleBase*> *particles, float posX, float posY, PixelBuffer *texture);
	~EmitterBase();

	 // to be called every frame
	void Update();

	// set position
	void SetPos(float _x, float _y);

protected:

	EmitterBase(); // can't call this!

	// emitter position
	float x,y;

	// pointer to particle list
	std::list<ParticleBase*> *particles;

	ParticleBase* CreateParticle();

	int life;

	// pointer to vector of textures
	PixelBuffer * texture;
};

#endif // EMITTER_BASE_CLASS
