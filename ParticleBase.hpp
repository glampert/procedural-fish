
// This project's source code is released under the MIT License.
// - http://opensource.org/licenses/MIT

/*
	ParticleBase originally written for ParticlePlayground assignment.

	This is a simplified version for CGA class
	2011 - 2
*/

#ifndef PARTICLE_BASE_CLASS
#define PARTICLE_BASE_CLASS

#include <stdlib.h>
#include <vector>

#include "GfxLib.hpp"
using namespace GfxLib;

struct PixelBuffer
{
	int width, height, bytesPerPix;
	unsigned char * pixels;
};

class ParticleBase
{
public:

	 ParticleBase();
	~ParticleBase();

	// function called every frame to control movement
	void Update(float deltaTime);

	 // config particle
	void ConfigParticle(float posX, float posY, float posZ, PixelBuffer *texture);

	// Get life from particle
	int GetLife() const { return life; }

private:

	// stores current position of the particle
	float x,y,z;

	// amount of frames that the particle still have left
	int life;

	// render particle
	void Render();

	PixelBuffer * texture;
};

#endif // PARTICLE_BASE_CLASS
