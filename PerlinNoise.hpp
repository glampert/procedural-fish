
// This project's source code is released under the MIT License.
// - http://opensource.org/licenses/MIT

#ifndef PERLIN_NOISE_HPP
#define PERLIN_NOISE_HPP

struct Pixel3ub
{
	unsigned char r, g, b;
};

static const int noiseImageHeight = 512;
static const int noiseImageWidth  = 512;
extern Pixel3ub noiseImage[];

float PerlinNoise(float x, float y, int width, int octaves, int seed, double periode);
void CreateNoiseImage(float scale, int width, int seed, unsigned char r, unsigned char g, unsigned char b);

#endif // PERLIN_NOISE_HPP
