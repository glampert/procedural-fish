
## Procedural fish simulation

A (very crude) procedural simulation of fish, based on flocking
behaviors and ["boids"](https://en.wikipedia.org/wiki/Boids). Class assignment.

This project only compiles under Windows. It was built using the Visual Studio 2008 IDE
and the old fixed-function OpenGL for rendering. **Project is no longer being maintained**.

----

This is how it looked like when running:

![Procedural Fish](https://bytebucket.org/glampert/procedural-fish/raw/17a523600969ec5982f9d4c5e4f40572f1792d0c/fishes.png "Procedural fish simulation")

----

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

