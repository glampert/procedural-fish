
// ===============================================================================================================
// -*- VERTEX SHADER -*-
//
// BinnPhong.vert - GLSL vertex shader.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

varying vec3 normalVec;

void main()
{
	// Pass the transformed vertices, normal vectors and texture coords for the next stage
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	gl_TexCoord[0] = gl_MultiTexCoord0;
	normalVec = gl_NormalMatrix * gl_Normal;
}
